import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Frame from '../Frame/Frame';

import './FrameContainer.css';

class FrameContainer extends Component {
    buildFrames = frames => {
        const renderedFrames = frames.map(frame => {
            return <Frame frame={frame} />;
        });
        return renderedFrames;
    };

    render() {
        const { frames } = this.props;
        const builtFrames = this.buildFrames(frames);
        return <div className="frame-container">{builtFrames}</div>;
    }
}

FrameContainer.propTypes = {
    frames: PropTypes.array.isRequired
};

export default FrameContainer;

import React from 'react';
import { shallow } from 'enzyme';
import PinIndicator from './PinIndicator';

const onePin = shallow(<PinIndicator number={1} />);

describe('PinIndicator', () => {
    it('should show the number passed into it', () => {
        expect(onePin.text()).toBe('1');
        const twoPin = shallow(<PinIndicator number={2} />);
        expect(twoPin.text()).toBe('2');
    });

    it('should have an icon as the background', () => {
        const icon = onePin.find('i');
        expect(icon.length).toBe(1);
    });

    it('should have icon class be "circle-o" and no "down" class before click', () => {
        const icon = onePin.find('i');
        const pinText = icon.find('#pinText');
        expect(icon.hasClass('fa-circle-o')).toBe(true);
        expect(pinText.hasClass('down')).toBe(false);
    });

    it('should have icon class be "circle" and "down" if isDown', () => {
        const onePinDown = shallow(<PinIndicator number={1} isDown={true} />);
        const icon = onePinDown.find('i');
        const pinText = icon.find('#pinText');
        expect(icon.hasClass('fa-circle')).toBe(true);
        expect(pinText.hasClass('down')).toBe(true);
    });
});

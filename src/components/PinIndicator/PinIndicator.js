import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './PinIndicator.css';

class PinIndicator extends Component {
    render() {
        const { number, onPinClick, isDown } = this.props;
        const iconClass = classNames(
            'fa fa-stack-2x',
            isDown ? 'fa-circle' : 'fa-circle-o'
        );
        const pinClass = classNames('fa-stack-1x pin', isDown && 'down');
        return (
            <span className="fa-stack">
                <i className={iconClass}>
                    <span
                        id="pinText"
                        className={pinClass}
                        onClick={onPinClick}
                    >
                        {number.toString()}
                    </span>
                </i>
            </span>
        );
    }
}

PinIndicator.propTypes = {
    number: PropTypes.number.isRequired,
    onPinClick: PropTypes.func.isRequired
};

export default PinIndicator;

// eslint globals describe, it, expect
import React from "react";
import { shallow } from "enzyme";
import AppHeader from "./AppHeader";

const appHeader = shallow(<AppHeader />);

describe("AppHeader", () => {
  it("should have a logo with className App-logo", () => {
    const logo = appHeader.find("img");
    expect(logo).toHaveLength(1);
    expect(logo.hasClass("App-logo")).toBe(true);
  });

  it("should have an h1 with className App-header", () => {
    const header = appHeader.find("h1");
    expect(header).toHaveLength(1);
    expect(header.hasClass("App-title")).toBe(true);
  });
});

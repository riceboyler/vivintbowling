import React, { Component } from 'react';
import logo from './logo.png';
import './AppHeader.css';

class AppHeader extends Component {
    render() {
        return (
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1 className="App-title">
                    The E-Bowl-aDrome (shout out to The Grand Tour)
                </h1>
            </header>
        );
    }
}

export default AppHeader;

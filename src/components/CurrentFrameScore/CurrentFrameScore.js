import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './CurrentFrameScore.css';

class CurrentFrameScore extends Component {
    render() {
        const { frame, ball, score } = this.props;
        return (
            <div className="current-score-container">
                <h3>
                    Current Frame: {frame} | Ball: {ball} | Total Score: {score}
                </h3>
            </div>
        );
    }
}

CurrentFrameScore.propTypes = {
    frame: PropTypes.number.isRequired,
    ball: PropTypes.number.isRequired,
    score: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default CurrentFrameScore;

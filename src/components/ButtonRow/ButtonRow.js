import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

import './ButtonRow.css';

class ButtonRow extends Component {
    render() {
        const {
            onDoneButtonClick,
            onGutterBallButtonClick,
            onStrikeButtonClick
        } = this.props;
        return (
            <div className="button-row">
                <div className="top-row">
                    <Button
                        outline
                        color="danger"
                        onClick={onGutterBallButtonClick}
                    >
                        No Pins
                    </Button>
                    <Button
                        outline
                        color="success"
                        onClick={onStrikeButtonClick}
                    >
                        All Pins
                    </Button>
                </div>
                <div className="bottom-row">
                    <Button color="primary" onClick={onDoneButtonClick}>
                        Done, Next Ball
                    </Button>
                </div>
            </div>
        );
    }
}

ButtonRow.propTypes = {
    onGutterBallButtonClick: PropTypes.func.isRequired,
    onStrikeButtonClick: PropTypes.func.isRequired,
    onDoneButtonClick: PropTypes.func.isRequired
};

export default ButtonRow;

import React from 'react';
import { shallow, mount } from 'enzyme';
import ButtonRow from './ButtonRow';
import { Button } from 'reactstrap';

const row = mount(<ButtonRow />);

describe('ButtonRow', () => {
    it('should have 3 buttons total', () => {
        expect(row.find(Button)).toHaveLength(3);
    });

    it('should have a Gutter Ball button', () => {
        expect(
            row
                .find(Button)
                .first()
                .text()
        ).toBe('No Pins');
    });

    it('should have a Strike! button', () => {
        const strikeButton = row.find('[color="success"]');
        expect(strikeButton.text()).toBe('All Pins');
    });

    it('should have a Done, Next Ball button', () => {
        const doneButton = row.find('[color="primary"]');
        expect(doneButton.text()).toBe('Done, Next Ball');
    });
});

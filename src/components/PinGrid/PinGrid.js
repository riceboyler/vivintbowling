import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PinIndicator from '../PinIndicator/PinIndicator';

class PinGrid extends Component {
    renderPinIndicators = () => {
        const { onPinClick, pinsDown } = this.props;
        const pinIndicators = [];
        for (let i = 1; i <= 10; i++) {
            const isDown = pinsDown.includes(i);
            pinIndicators.push(
                <PinIndicator
                    key={i}
                    number={i}
                    onPinClick={onPinClick}
                    isDown={isDown}
                />
            );
            switch (i) {
                case 1:
                case 3:
                case 6:
                case 10:
                    pinIndicators.push(<br key={`br${i}`} />);
                    break;
                default:
                    break;
            }
        }
        return pinIndicators;
    };
    render() {
        const pinIndicators = this.renderPinIndicators();
        return <div>{pinIndicators}</div>;
    }
}

PinGrid.propTypes = {
    onPinClick: PropTypes.func.isRequired,
    pinsDown: PropTypes.array
};

export default PinGrid;

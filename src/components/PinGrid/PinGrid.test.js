import React from 'react';
import { shallow, mount } from 'enzyme';
import PinGrid from './PinGrid';
import PinIndicator from '../PinIndicator/PinIndicator';

const grid = mount(<PinGrid pinsDown={[]} />);

describe('PinGrid', () => {
    it('should contain a number 1 PinIndicator', () => {
        expect(grid.containsMatchingElement(<PinIndicator number={1} />)).toBe(
            true
        );
    });

    it('should contain 10 PinIndicators', () => {
        const pinIndicators = grid.find(PinIndicator);
        expect(pinIndicators).toHaveLength(10);
    });

    it('should contain 4 <br /> elements', () => {
        const theseAreTheBreaks = grid.find('br');
        expect(theseAreTheBreaks).toHaveLength(4);
    });
});

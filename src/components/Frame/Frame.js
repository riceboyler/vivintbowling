import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Frame.css';

class Frame extends Component {
    render() {
        const { frame } = this.props;
        return (
            <div className="frame">
                <span className="frame-number">{frame.frameNumber}</span>
                <span className="frame-score">{frame.score}</span>
                <span className="ball1-score">{frame.ball1Pins.length}</span>
                <span className="ball2-score">{frame.ball2Pins.length}</span>
            </div>
        );
    }
}

Frame.propTypes = {};

export default Frame;

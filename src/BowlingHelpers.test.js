import * as BowlingHelpers from './BowlingHelpers';

const allPins = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const firstFrameFirstBall = {
    frameNumber: 1,
    ball1Pins: [1, 2, 3, 4, 5, 6],
    ball2Pins: [],
    ball3Pins: [],
    score: 6
};

describe('Bowling Helpers', () => {
    describe('getNextFrameBall', () => {
        it('should return next ball as 2 if current ball is 1 in a frame and is not a strike', () => {
            const result = BowlingHelpers.getNextFrameBall(1, 1, []);
            expect(result.nextFrame).toBe(1);
            expect(result.nextBall).toBe(2);
        });

        it('should return next ball as 1 and next frame as 2 if current ball is 1 and is a strike', () => {
            const result = BowlingHelpers.getNextFrameBall(1, 1, allPins);
            expect(result.nextFrame).toBe(2);
            expect(result.nextBall).toBe(1);
        });

        it('should return next frame as 2 if current ball is 2', () => {
            const result = BowlingHelpers.getNextFrameBall(1, 2, 0);
            expect(result.nextFrame).toBe(2);
            expect(result.nextBall).toBe(1);
        });

        it('should return next ball as 2 if current ball is 1 and frame is 10', () => {
            const result = BowlingHelpers.getNextFrameBall(10, 1, 0);
            expect(result.nextFrame).toBe(10);
            expect(result.nextBall).toBe(2);
        });

        it('should return next ball as 0 if current frame is 10, ball is 2 and pinsDown is less than 10', () => {
            const result = BowlingHelpers.getNextFrameBall(10, 2, 8);
            expect(result.nextFrame).toBe(0);
            expect(result.nextBall).toBe(0);
        });

        it('should return next ball as 3 if current frame is 10, ball is 2 and pinsDown is 10', () => {
            const result = BowlingHelpers.getNextFrameBall(10, 2, 10);
            expect(result.nextFrame).toBe(10);
            expect(result.nextBall).toBe(3);
        });
    });

    describe('handleScoring', () => {
        it('should return score as "X" for a strike in the first frame', () => {
            const result = BowlingHelpers.handleScoring(1, 1, allPins, []);
            expect(result).toHaveLength(1);
            expect(result[0].score).toBe('X');
        });
        it('should return score as "/" for a spare in the first frame', () => {
            const frames = [firstFrameFirstBall];
            const result = BowlingHelpers.handleScoring(
                1,
                2,
                [7, 8, 9, 10],
                frames
            );
            expect(result).toHaveLength(1);
            expect(result[0].score).toBe('/');
        });
        it('should return score as 9 for 6 pins on first ball and 3 pins on second ball in first frame', () => {
            const frames = [firstFrameFirstBall];
            const result = BowlingHelpers.handleScoring(
                1,
                2,
                [1, 2, 3, 4, 5, 6, 8, 9, 10],
                frames
            );
            expect(result).toHaveLength(1);
            expect(result[0].score).toBe(9);
        });
        it('should return score for first frame as 20 for spare on first frame and spike on second frame', () => {
            firstFrameFirstBall.ball2Pins = [7, 8, 9, 10];
            firstFrameFirstBall.score = '/';
            const frames = [firstFrameFirstBall];
            const result = BowlingHelpers.handleScoring(2, 1, allPins, frames);
            expect(result).toHaveLength(2);
            expect(result[0].score).toBe(20);
            expect(result[1].score).toBe('X');
        });
        it('should score 3 strikes in a row as 30 points in the first frame', () => {
            const firstFrame = firstFrameFirstBall;
            firstFrame.ball1Pins = allPins;
            firstFrame.score = 'X';

            const secondFrame = Object.assign({}, firstFrame);
            secondFrame.frameNumber = 2;

            const frames = [firstFrame, secondFrame];
            const result = BowlingHelpers.handleScoring(3, 1, allPins, frames);
            expect(result).toHaveLength(3);
            expect(result[0].score).toBe(30);
            expect(result[1].score).toBe('X');
            expect(result[2].score).toBe('X');
        });

        it('should score 2 strikes and a non-strike as 23 points in the first frame and leave "X" in the second frame', () => {
            const firstFrame = firstFrameFirstBall;
            firstFrame.ball1Pins = allPins;
            firstFrame.score = 'X';

            const secondFrame = Object.assign({}, firstFrame);
            secondFrame.frameNumber = 2;

            const frames = [firstFrame, secondFrame];
            const result = BowlingHelpers.handleScoring(
                3,
                1,
                [1, 2, 3],
                frames
            );
            expect(result).toHaveLength(3);
            expect(result[0].score).toBe(23);
            expect(result[1].score).toBe('X');
            expect(result[2].score).toBe(3);
        });
    });
});

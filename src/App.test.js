import React from 'react';
import { shallow, mount } from 'enzyme';
import App from './App';
import AppHeader from './components/AppHeader/AppHeader';
import PinGrid from './components/PinGrid/PinGrid';
import ButtonRow from './components/ButtonRow/ButtonRow';

const app = mount(<App />);

it('renders without crashing', () => {
    shallow(<App />);
});

it('should have an AppHeader component', () => {
    expect(app.containsMatchingElement(<AppHeader />)).toBe(true);
});

it('should have a PinGrid component', () => {
    expect(app.containsMatchingElement(<PinGrid />)).toBe(true);
});

it('should have a ButtonRow component', () => {
    expect(app.containsMatchingElement(<ButtonRow />)).toBe(true);
});

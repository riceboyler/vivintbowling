import React, { Component } from 'react';
import './App.css';
import AppHeader from './components/AppHeader/AppHeader';
import PinGrid from './components/PinGrid/PinGrid';
import ButtonRow from './components/ButtonRow/ButtonRow';
import CurrentFrameScore from './components/CurrentFrameScore/CurrentFrameScore';
import * as BowlingHelpers from './BowlingHelpers';
import FrameContainer from './components/FrameContainer/FrameContainer';

class App extends Component {
    state = {
        frame: 1,
        ball: 1,
        pinsDown: [],
        score: 0,
        frames: []
    };

    onDoneButtonClick = () => {
        const { frame, ball, pinsDown, frames } = this.state;
        const nextFrameBall = BowlingHelpers.getNextFrameBall(
            frame,
            ball,
            pinsDown
        );
        const scoredFrames = BowlingHelpers.handleScoring(
            frame,
            ball,
            pinsDown,
            frames
        );
        const totalScore = BowlingHelpers.getTotalScore(frames);
        this.setState({
            frame: nextFrameBall.nextFrame,
            ball: nextFrameBall.nextBall,
            pinsDown: nextFrameBall.nextBall === 1 ? [] : pinsDown,
            frames: scoredFrames,
            score: totalScore
        });
    };

    onStrikeButtonClick = () => {
        this.setState({ pinsDown: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] });
    };

    onGutterBallButtonClick = () => {
        this.setState({ pinsDown: [] });
    };

    onPinClick = evt => {
        evt.preventDefault;
        const pinNumber = parseInt(evt.target.innerText, 10);
        const { pinsDown } = this.state;
        const pinIndex = pinsDown.includes(pinNumber);
        let nextPinsDown = [];
        if (pinIndex) {
            nextPinsDown = Object.assign([], pinsDown);
            nextPinsDown.splice(pinNumber - 1, 1);
        } else {
            nextPinsDown = pinsDown.concat([pinNumber]);
        }
        this.setState({ pinsDown: nextPinsDown });
    };

    render() {
        const { pinsDown, frame, ball, score, frames } = this.state;
        return (
            <div className="App">
                <AppHeader />
                <div className="body-container">
                    <h3>
                        Select the pins below that were knocked down on this
                        ball
                    </h3>
                    <PinGrid pinsDown={pinsDown} onPinClick={this.onPinClick} />
                    <ButtonRow
                        onDoneButtonClick={this.onDoneButtonClick}
                        onStrikeButtonClick={this.onStrikeButtonClick}
                        onGutterBallButtonClick={this.onGutterBallButtonClick}
                    />
                    <CurrentFrameScore
                        frame={frame}
                        ball={ball}
                        score={score}
                    />
                    <FrameContainer frames={frames} />
                </div>
            </div>
        );
    }
}

export default App;

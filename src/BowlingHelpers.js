import _ from 'lodash';

export function getNextFrameBall(currentFrame, currentBall, pinsDown) {
    let nextFrame, nextBall;
    if (currentFrame < 10) {
        if (currentBall === 1) {
            if (pinsDown.length < 10) {
                nextFrame = currentFrame;
                nextBall = currentBall + 1;
            } else {
                nextFrame = currentFrame + 1;
                nextBall = 1;
            }
        } else {
            nextFrame = currentFrame + 1;
            nextBall = 1;
        }
    } else {
        nextBall = handleTenthFrameMadness(currentBall, pinsDown);
        nextFrame = nextBall === 0 ? 0 : currentFrame;
    }
    return {
        nextFrame,
        nextBall
    };
}

function handleTenthFrameMadness(currentBall, pinsDown) {
    let nextBall;
    switch (currentBall) {
        case 1:
            return pinsDown === 10 ? 3 : 2;
        case 2:
            return pinsDown === 10 ? 3 : 0;
        case 3:
        default:
            return 0;
    }
}

export function handleScoring(currentFrame, currentBall, pinsDown, frames) {
    let score = 0;
    let newFrames = [];
    let frame;
    if (currentBall > 1) {
        frame = frames[currentFrame - 1];
        frame.score = getFrameScore(currentBall, frame, pinsDown);
        frames.splice(-1, 1, frame);
        newFrames = Object.assign([], frames);
    } else {
        frame = {
            frameNumber: currentFrame,
            ball1Pins: [],
            ball2Pins: [],
            ball3Pins: [],
            score: 0
        };
        frame.score = getFrameScore(currentBall, frame, pinsDown);
        newFrames = frames.concat([frame]);
    }

    const scoredFrames = recalculateAllScores(newFrames, currentBall);
    return scoredFrames;
}

function getFrameScore(currentBall, frame, pinsDown) {
    let score = 0;
    if (currentBall === 1) {
        frame.ball1Pins = pinsDown;
    } else if (currentBall === 2) {
        frame.ball2Pins = _.difference(pinsDown, frame.ball1Pins);
    } else {
        frame.ball3Pins = _.difference(
            pinsDown,
            frame.ball1Pins + frame.ball2Pins
        );
    }
    const framePinsDown =
        frame.ball1Pins.length +
        frame.ball2Pins.length +
        frame.ball3Pins.length;

    if (framePinsDown === 10) {
        if (currentBall === 1) {
            //Strike!
            score = 'X';
        } else {
            //Spare
            score = '/';
        }
    } else {
        score = framePinsDown;
    }
    return score;
}

function recalculateAllScores(frames, currentBall) {
    frames.forEach((frame, index) => {
        const frameThirdBall = frame.ball3Pins.length;
        if (currentBall < 3) {
            frame.score = processFrameScore(frames, currentBall, frame, index);
        } else {
            // Only the 10th frame has a 3rd ball, and by default it's only to determine what a potential previous strike/spare value would be, so its value is just the number of pins
            frame.score = frameThirdBall;
        }
    });
    return frames;
}

function processFrameScore(frames, currentBall, frame, index) {
    const nextFrame = frames[index + 1];
    const nextNextFrame = frames[index + 2];
    const frameFirstBall = frame.ball1Pins.length;
    const frameSecondBall = frame.ball2Pins.length;
    let score;

    if (frameFirstBall === 10) {
        //Strike!
        score = processStrikeScore(
            nextFrame,
            nextNextFrame,
            frames,
            currentBall
        );
    } else if (frameFirstBall + frameSecondBall === 10) {
        //Spare
        score = processSpareScore(nextFrame);
    } else {
        if (frame.score !== 'X' && frame.score !== '/') {
            score = frameFirstBall + frameSecondBall;
        }
    }
    return score;
}

function processSpareScore(nextFrame) {
    let score = '/';
    if (nextFrame) {
        const nextFrameFirstBall = nextFrame.ball1Pins.length;
        if (nextFrameFirstBall === 10) {
            score = 20;
        } else {
            score = 10 + nextFrameFirstBall;
        }
    }
    return score;
}

function processStrikeScore(nextFrame, nextNextFrame, frames, currentBall) {
    let score = 'X';
    if (nextFrame) {
        const nextFrameFirstBall = nextFrame.ball1Pins.length;
        const nextFrameSecondBall = nextFrame.ball2Pins.length;
        const nextFrameSum = nextFrameFirstBall + nextFrameSecondBall;
        if (nextFrameFirstBall === 10) {
            //Two strikes in a row, have to go to the next frame
            if (nextNextFrame) {
                const nextNextFrameFirstBall = nextNextFrame.ball1Pins.length;
                if (nextNextFrameFirstBall === 10) {
                    score = 30;
                } else {
                    score = 20 + nextNextFrameFirstBall;
                }
            }
        } else if (nextFrameSum === 10) {
            score = 20;
        } else {
            if (currentBall > 1) {
                score = 10 + nextFrameSum;
            }
        }
    }
    return score;
}

export function getTotalScore(frames) {
    let score = 0;
    frames.forEach(frame => {
        if (frame.score !== 'X' && frame.score !== '/') {
            score += frame.score;
        }
    });
    return score;
}

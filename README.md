# Vivint Coding Exercise

* Built with `create-react-app` to get React up and running quickly
* Utilized `reactstrap` to not have to rewrite basic Bootstrap 4 components
* Using `yarn` rather than npm due to performance
* I eschewed Redux in this case because it initially felt like overkill, and I wanted to show it is possible to use state and one-way data flow with a traditional event model. Were this a production app, I'd likely have included Redux due to different components needing access to the same store.

To run:

1. If `yarn` isn't installed, I recommend you give it a shot. I bet you'll like it. :)
2. Run `yarn` to install all dependencies.
3. To run tests: `yarn run test`. At this point, all tests should pass.
4. To see UI: `yarn start` which should open a browser and load the page.

## Notes

This isn't done, but I figured it was better to send you guys a pile of code to look at, rather than say I didn't have time to finish it. The Scoring Logic is a little flawed, the frame display is not even close to where I want it and there's an error from time to time with entering a second ball in a frame. The tenth frame logic isn't built, and the tenth frame UI isn't built.

One note about the tests: yeah, I wrote a good number of tests, and what I found, especially for the BowlingHelpers file, they were a huge time saver as they helped me find a number of issues that would have been much harder to track down merely via the UI.
